"""
    Performs data exploration on provided dataset
"""

import json
import logging
import os
import time
import sys

import click
from xpresso.ai.client.data_client import config
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InvalidDatatypeException, CLICommandFailedException
from xpresso.ai.core.commons.utils.constants import KEY_RUN_NAME, \
    PARAMETERS_COMMIT_ID_KEY, PARAMETERS_FILENAME_KEY
from xpresso.ai.core.commons.utils.generic_utils import move_directory
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.automl.structured_dataset import StructuredDataset
from xpresso.ai.core.data.automl.unstructured_dataset import UnstructuredDataset
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__all__ = ["DataConnector"]
__author__ = "Ashritha Goramane"

logger = XprLogger("data_connector", level=logging.INFO)


class DataConnector(AbstractPipelineComponent):
    """
        To import data from different source using data connector
    """
    COMPONENT_NAME = "component_name"
    DATASET_TYPE = 'dataset_type'
    DATA_CONFIG = "data_config"

    CONNECTOR_OUTPUT_PATH = "connector_output_path"
    OPTIONS = "options"
    DATA_CONFIG_TYPE = "data_config_type"
    DATA_CONFIG_PATH = "data_config_path"
    DATA_CONFIG_DATA_SOURCE = "data_config_data_source"
    DATA_CONFIG_TABLE = "data_config_table"
    DATA_CONFIG_COLUMNS = "data_config_columns"
    DATA_CONFIG_DSN = "data_config_dsn"
    FILE_NAME = "file_name"
    DATASET_NAME = "dataset_name"
    DESCRIPTION = "description"
    PROJECT_NAME = "project_name"
    CREATED_BY = "created_by"

    def __init__(self, xpresso_run_name, parameters_filename,
                 parameters_commit_id, **kwargs):
        super().__init__(name="DataVersioning", run_name=xpresso_run_name,
                         params_filename=parameters_filename,
                         params_commit_id=parameters_commit_id)
        # if you have specified parameters_filename or parameters_commit_id,
        # the run parameters can be accessed from self.run_parameters
        self.cli_args = {}
        self.arguments = kwargs
        self.fetch_arguments()
        self.validate_arguments()
        self.repomanager = None
        self.xpr_con = None
        self.config = config
        self.dataset = None
        self.explorer = None
        self.start_timestamp = None
        self.end_timestamp = None

    def start(self, xpresso_run_name):
        """
        This is the start method, which does the actual data
        preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the
          Controller that
              the component has started processing (details such as
              the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            xpresso_run_name: xpresso run name which is used by base class to
                identify the current run. It must be passed. While running as
                pipeline,
               Xpresso automatically adds it.
        """
        try:
            super().start(xpresso_run_name=xpresso_run_name)
            self.start_timestamp = time.time()
            print("Data connector component starting", flush=True)
            self.import_dataset()
            self.end_timestamp = time.time()
            print("Data connector component completed", flush=True)
            self.send_metrics("import_dataset")
        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(success=True)

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the
        end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases
        """
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def import_dataset(self):
        """Import dataset as structured or unstructured using data connector"""
        try:
            print("Importing dataset", flush=True)
            self.get_dataset_object()
            self.set_data_config()
            print(self.cli_args[self.DATA_CONFIG])
            logger.info(
                f"Importing dataset with {self.cli_args[self.DATA_CONFIG]}")
            self.dataset.import_dataset(self.cli_args[self.DATA_CONFIG])
            try:
                if self.dataset.data.empty:
                    print("Unable to import dataset with given config")
            except AttributeError:
                print(
                    "Given file path doesn't exists. Unable to import dataset with given "
                    "config")
            self.save_dataset()
        except Exception as exp:
            import traceback
            traceback.print_exc()
            raise exp

    def set_data_config(self):
        """Set data config from arguments"""
        if self.cli_args[self.DATA_CONFIG]:
            return
        data_config = dict()
        config_arguments = {self.DATA_CONFIG_TYPE: "type",
                            self.DATA_CONFIG_DATA_SOURCE: "data_source",
                            self.DATA_CONFIG_PATH: "path",
                            self.DATA_CONFIG_TABLE: "table",
                            self.DATA_CONFIG_COLUMNS: "columns",
                            self.DATA_CONFIG_DSN: "DSN"}
        print(self.cli_args[self.DATA_CONFIG_DSN])
        for key, value in config_arguments.items():
            if self.cli_args[key]:
                data_config[value] = self.cli_args[key]
        self.cli_args[self.DATA_CONFIG] = data_config

    def get_dataset_object(self):
        """Depending upon dataset type get specific dataset object"""
        dataset_type = self.cli_args[self.DATASET_TYPE]
        dataset_name = self.cli_args[self.DATASET_NAME] if self.cli_args[
            self.DATASET_NAME] else "default_dataset"
        description = self.cli_args[self.DESCRIPTION] if self.cli_args[
            self.DESCRIPTION] else "Description"
        project_name = self.cli_args[self.PROJECT_NAME] if self.cli_args[
            self.PROJECT_NAME] else "default_project"
        created_by = self.cli_args[self.CREATED_BY] if self.cli_args[
            self.CREATED_BY] else "default"

        if dataset_type.lower() == DatasetType.STRUCTURED.value:
            self.dataset = StructuredDataset(dataset_name=dataset_name,
                                             project_name=project_name,
                                             created_by=created_by,
                                             description=description)
        elif dataset_type.lower() == DatasetType.UTEXT.value or \
                dataset_type.lower() == DatasetType.UNSTRUCTURED.value:
            self.dataset = UnstructuredDataset(dataset_name=dataset_name,
                                               project_name=project_name,
                                               created_by=created_by,
                                               description=description)
        else:
            raise InvalidDatatypeException(f"Dataset type {dataset_type} not "
                                           "supported")

    def send_metrics(self, status):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": status},
                "metric": {
                    "elapsed_time": self.end_timestamp - self.start_timestamp}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def save_dataset(self):
        """Save dataset into out path"""
        folder_path = self.dataset.save()
        logger.info(
            f"Saving dataset to {self.cli_args[self.CONNECTOR_OUTPUT_PATH]}")
        move_directory(folder_path, self.cli_args[self.CONNECTOR_OUTPUT_PATH])
        print(f"Data saved to {self.cli_args[self.CONNECTOR_OUTPUT_PATH]}")
        if self.cli_args[self.FILE_NAME]:
            output_path = os.path.join(self.cli_args[self.CONNECTOR_OUTPUT_PATH],
                                       self.cli_args[self.FILE_NAME])
            print(f"Saving data csv into {output_path}")
            self.dataset.data.to_csv(output_path, index=False)

    def extract_argument(self, argument):
        """
        Args:
            argument(str): Name of argument to extract
        Returns:
            argument value or None
        """
        if argument in self.arguments:
            return self.arguments[argument]
        return None

    def fetch_arguments(self):
        """
        Fetch arguments form CLI
        Returns:
            Returns arguments
        """
        arguments_key = [self.COMPONENT_NAME, self.DATASET_TYPE,
                         self.DATA_CONFIG_PATH,
                         self.CONNECTOR_OUTPUT_PATH, self.DATA_CONFIG_TYPE,
                         self.DATA_CONFIG_DATA_SOURCE, self.DATA_CONFIG_DSN,
                         self.DATA_CONFIG_TABLE, self.DATA_CONFIG_COLUMNS,
                         self.DATA_CONFIG, self.OPTIONS, self.FILE_NAME,
                         self.DATASET_NAME, self.DESCRIPTION, self.PROJECT_NAME,
                         self.CREATED_BY]

        for arg in arguments_key:
            if arg in self.run_parameters:
                self.cli_args[arg] = self.run_parameters[arg]
                continue
            self.cli_args[arg] = self.extract_argument(arg)

        try:
            if self.cli_args[self.DATA_CONFIG]:
                self.cli_args[self.DATA_CONFIG] = json.loads(
                    self.cli_args[self.DATA_CONFIG])
        except json.JSONDecodeError as e:
            raise CLICommandFailedException(
                f"Invalid JSON file provided as data_config. \n"
                f"Error on line number {e.lineno} : "
                f"'{e.msg}'.\n"
                f"Please check your file's formatting.")

        try:
            if self.cli_args[self.OPTIONS]:
                self.cli_args[self.OPTIONS] = json.loads(
                    self.cli_args[self.OPTIONS])
        except json.JSONDecodeError as e:
            raise CLICommandFailedException(
                f"Invalid JSON file provided as options. \n"
                f"Error on line number {e.lineno} : "
                f"'{e.msg}'.\n"
                f"Please check your file's formatting.")

    def validate_arguments(self):
        if not self.cli_args[self.COMPONENT_NAME]:
            raise CLICommandFailedException("Actual component name mandatory")
        self.name = self.cli_args[self.COMPONENT_NAME]
        if not self.cli_args[self.CONNECTOR_OUTPUT_PATH]:
            self.cli_args[self.CONNECTOR_OUTPUT_PATH] = '/data'


@click.command()
@click.argument(KEY_RUN_NAME)
@click.argument(PARAMETERS_FILENAME_KEY)
@click.argument(PARAMETERS_COMMIT_ID_KEY)
@click.option('-component-name', type=str,
              help='Actual component name specified during create project.')
@click.option('-connector-output-path', type=str, help='Path of the file to '
                                                      'load data from')
@click.option('-data-config-type', type=str, help='Source where to pick data '
                                                  'from')
@click.option('-data-config-path', type=str,
              help='Path of the file to load data from')
@click.option('-data-config-dsn', type=str,
              help='Data Source Name. For reference refer to '
                   'Data Source Connectivity document'
              )
@click.option('-data-config-data-source', type=str,
              help='Data source of the file to load '
                   'data from')
@click.option('-data-config-table', type=str,
              help='Name of the table in presto data source')
@click.option('-data-config-columns', type=str,
              help='Name of column inside the table '
                   'stored in presto data source')
@click.option('-data-config', type=str,
              help='Data configuration to load data from Eg. -i \'{"uid": '
                   '"abc"}\')')
@click.option('-data-config-options', type=str,
              help='Extra keyword arguments to be specified as key-value pair,'
                   ' for better importing through files. Eg.\'{"type":"FS", '
                   '"path":"/test_connector/small_size_data/test.csv"}\'')
@click.option('-dataset-type', type=str, help='Dataset type (i.e structured, '
                                              'unstructured)')
@click.option('-file-name', type=str, help='File name for the csv file')
@click.option('-dataset-name', type=str, help='Name of the dataset to be '
                                              'stored')
@click.option('-description', type=str, help='Description of the dataset1')
@click.option('-project-name', type=str, help='Name of the project')
@click.option('-created-by', type=str, help='Dataset created by ')
def cli_options(**kwargs):
    run_name = ""
    params_filename = None
    params_commit_id = None
    if KEY_RUN_NAME in kwargs:
        run_name = kwargs[KEY_RUN_NAME]
        kwargs.pop(KEY_RUN_NAME)
    if PARAMETERS_FILENAME_KEY in kwargs and PARAMETERS_COMMIT_ID_KEY in kwargs:
        params_filename = kwargs[PARAMETERS_FILENAME_KEY] \
            if kwargs[PARAMETERS_FILENAME_KEY] != "None" else None
        kwargs.pop(PARAMETERS_FILENAME_KEY)
        params_commit_id = kwargs[PARAMETERS_COMMIT_ID_KEY] \
            if kwargs[PARAMETERS_COMMIT_ID_KEY] != "None" else None
        kwargs.pop(PARAMETERS_COMMIT_ID_KEY)
    try:
        pipeline_job = DataConnector(run_name, params_filename,
                                     params_commit_id, **kwargs)
        pipeline_job.start(xpresso_run_name=run_name)
    except Exception as exception:
        click.secho(f"Error:{exception}", err=True, fg="red")


if __name__ == "__main__":
    cli_options()
